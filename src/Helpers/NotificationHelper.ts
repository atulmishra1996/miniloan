export function createNotificationMetaData(loanId: string, loanAmount: bigint, loanTenure: string){
    return {
        loanId: loanId,
        loanAmount: loanAmount,
        loanTenure: loanTenure
    }
}