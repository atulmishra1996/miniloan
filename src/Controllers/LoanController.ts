import LoanService from "../Services/LoanService.js";
import {FastifyReply, FastifyRequest} from "fastify";

export default class LoanController {

    constructor(public loanService: LoanService) {
    }

    async applyLoan(request: FastifyRequest, reply: FastifyReply): Promise<void> {
        try {

            const {organisationId, userId, loanAmount, loanTenure, loanCurrency} =
                request.body as {
                    organisationId: string,
                    userId: string,
                    loanAmount: bigint,
                    loanTenure: string,
                    loanCurrency: string
                };


            const result = await this.loanService.applyLoan(organisationId, userId, loanAmount, loanTenure, loanCurrency);

            // @ts-ignore
            reply.code(201).send({ message: 'Loan Successfully created', loanId: result.loanId});
        } catch (err){
            reply.code(200).send({ message: 'Loan Creation Failed' + err.message});
        }

    }

    async approveLoan(request: FastifyRequest, reply: FastifyReply): Promise<void> {
        try {
            const {loanId, approverId, approverRemarks} =
                request.body as {
                    loanId: string,
                    approverId: string,
                    approverRemarks: string
                };

            const result = await this.loanService.approveLoan(loanId, approverId, approverRemarks);

            // @ts-ignore
            reply.code(201).send({ message: 'Loan Approved Successfully', loanId: result.loanId});
        } catch (err){
            reply.code(200).send({ message: 'Loan Approval Failed' + err.message});
        }
    }


    async rejectLoan(request: FastifyRequest, reply: FastifyReply): Promise<void> {
        try {
            const {loanId, approverId, approverRemarks} =
                request.body as {
                    loanId: string,
                    approverId: string,
                    approverRemarks: string
                };

            const result = await this.loanService.rejectLoan(loanId, approverId, approverRemarks);

            // @ts-ignore
            reply.code(201).send({ message: 'Loan Rejected Successfully', loanId: result.loanId});
        } catch (err){
            reply.code(200).send({ message: 'Loan Rejection Failed' + err.message});
        }
    }
}