import {FastifyInstance, FastifyReply, FastifyRequest} from "fastify";
import {OrganisationService} from "../Services/OrganisationService.js";
import {json} from "sequelize";

export default class OrganisationController {
    constructor(public organisationService: OrganisationService) {
    }

    async create(request: FastifyRequest, reply: FastifyReply): Promise<void> {
        console.log("Getting Request :: " + request.body);
        const { organisationName, organisationDescription, userName, email } =
            request.body as {
                organisationName: string,
                organisationDescription: string,
                userName: string,
                email: string
            };

        try {
          const organisation = await this.organisationService.createOrganisation(organisationName, organisationDescription);
          const adminOnboarded = await this.organisationService.onboardNewUser(userName, email, 'admin', organisation.orgId);
            console.log("Result  :: " , organisation);
            reply.code(201).send({ message: 'Organisation and admin user created successfully.' });
        } catch (err) {
            reply.code(500).send(err);
        }
    }
}