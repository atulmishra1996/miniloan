import NotificationService from "../Services/NotificationService.js";
import {FastifyReply, FastifyRequest} from "fastify";

export default class NotificationController {

    constructor(public notificationService: NotificationService) {
    }

    async fetchNotifications(request: FastifyRequest, reply: FastifyReply): Promise<void> {
        try {
            const notifications = await this.notificationService.fetchNotificationsByUser('12213');
            // @ts-ignore
            reply.code(201).send({ message: 'Successfully Fetched User Notifications', notifications: notifications});
        } catch (err) {
            reply.code(200).send({ message: 'Failed to fetch user notifications' + err.message});
        }
    }
}