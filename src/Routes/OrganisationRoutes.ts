import OrganisationController from "../Controllers/OrganisationController.js";

export default class OrganisationRoutes {
    /**
     * @constructor
     *
     * @param {OrganisationController} organisationController
     */
    constructor(public organisationController: OrganisationController) {
    }

    async routes(server: any) {

        server.post(
            "/organisation",
            {
                schema: {
                    summary: "Creates a new Organisation and onboards first admin",
                    body: {
                        type: "object",
                        properties: {
                            organisationName: {type: "string"},
                            organisationDescription: {type: "string"},
                            userName: {type: "string"},
                            email: {type: "string"}
                        }
                    },
                },
            },
            this.organisationController.create.bind(this.organisationController)
        );
    }
}