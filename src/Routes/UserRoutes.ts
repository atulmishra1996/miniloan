import UsersController from "../Controllers/UsersController.js";

export default class UserRoutes {
    constructor(public userController: UsersController) {
    }

    async routes(server: any) {

        server.post(
            "/signUp",
            {
                schema: {
                    summary: "Creates a new User for an Organisation",
                    body: {
                        type: "object",
                        properties: {
                            organisationId: {type: "string"},
                            userName: {type: "string"},
                            email: {type: "string"},
                            password: {type: "string"},
                            type: {type: "string"}
                        }
                    },
                },
            },
            this.userController.signUp.bind(this.userController)
        );


        server.post(
            "/logIn",
            {
                schema: {
                    summary: "LogIn User",
                    body: {
                        type: "object",
                        properties: {
                            email: {type: "string"},
                            password: {type: "string"}
                        }
                    },
                },
            },
            this.userController.logIn.bind(this.userController)
        );
    }
}