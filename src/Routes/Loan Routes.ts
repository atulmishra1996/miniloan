import LoanController from "../Controllers/LoanController.js";

export default class LoanRoutes {

    constructor(public loanController: LoanController) {
    }

    async routes(server: any) {

        server.post(
            "/loans/apply",
            {
                schema: {
                    summary: "Creates a new loan for the user",
                    body: {
                        type: "object",
                        properties: {
                            organisationId: {type: "string"},
                            userId: {type: "string"},
                            loanAmount: {type: "integer"},
                            loanTenure: {type: "string"},
                            loanCurrency: {type: "string"}
                        }
                    },
                },
            },
            this.loanController.applyLoan.bind(this.loanController)
        );

        server.put(
            "/loans/approve",
            {
                schema: {
                    summary: "Approves Loan by a approver",
                    body: {
                        type: "object",
                        properties: {
                            loanId: {type: "string"},
                            approverId: {type: "string"},
                            approverRemarks: {type: "string"}
                        }
                    },
                },
            },
            this.loanController.approveLoan.bind(this.loanController)
        );


        server.put(
            "/loans/reject",
            {
                schema: {
                    summary: "Reject Loans by a approver",
                    body: {
                        type: "object",
                        properties: {
                            loanId: {type: "string"},
                            approverId: {type: "string"},
                            approverRemarks: {type: "string"}
                        }
                    },
                },
            },
            this.loanController.rejectLoan.bind(this.loanController)
        );
    }
}