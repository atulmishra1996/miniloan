import NotificationController from "../Controllers/NotificationController.js";

export default class NotificationRoutes {

    constructor(public notificationController: NotificationController) {
    }

    async routes(server: any) {

        server.get(
            "/notifications",
            {
                schema: {
                    summary: "Fetch all notification for a user",
                },
            },
            this.notificationController.fetchNotifications.bind(this.notificationController)
        );
    }
}