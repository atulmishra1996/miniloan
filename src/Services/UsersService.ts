import {User} from "../Models/Users.js";
import bcrypt from 'bcrypt';
import jwt from "fastify-jwt"
export default class UsersService {

    constructor() {
    }

    async signUp(userName: string, password: string, type: string, orgId: string, email: string): Promise<{ message: string} | Error> {
        try {

            const isExistingUser = await User.findOne({
                where: {email: email}
            })

            if(isExistingUser){
                throw new Error("User Already Exist");
            }

            // HAshing User Password
            const saltRounds = 10;
            const salt = await bcrypt.genSalt(saltRounds);
            const hashedPasswords = await bcrypt.hash(password, salt);

            console.log("Trying to create new user with :: " + userName + " - " + password + " - " + type + " - " + orgId, + "  " + email)
            const user = await User.create({
                name: userName,
                password: hashedPasswords,
                type: type,
                status: 'pending',
                organisationID: orgId,
                email: email
            });

            console.log("Inserting new user :: " + user);
            return {message: "User Created Successfully "};
        } catch (e){
            console.log("Failed to insert into User  ", e);
            throw new Error("User Creation Failed  - " + e.message);
        }
    }

    async login(email: string, password: string): Promise<{message:string, token: string} | Error>{
        try {
            const isExistingUser = await User.findOne({
                where: {email: email}
            })

            if (!isExistingUser) {
                throw new Error("User Doesn't Exist");
            }

            console.log("Service : User exist :: ");

            // @ts-ignore
            const isPasswordValid = await bcrypt.compare(password, isExistingUser.password);

            console.log(" Password Matched " + isPasswordValid);
            if (isPasswordValid) {
                // @ts-ignore
                const token = jwt.sign({ userId: isExistingUser.id, orgId: isExistingUser.organisationID, email: isExistingUser.email}, 'dajndakjbdyabdkhsabkjdsad', { expiresIn: '12h' });

                return ({ message: 'Login successful', token: token});
            } else {
                throw new Error('Invalid credentials');
            }

        } catch (e){
            console.log("LoginService::  Catched Exception :: " + e);
            throw new Error(e);
        }
    }

}