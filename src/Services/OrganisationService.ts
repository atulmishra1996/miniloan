import fastify, {FastifyInstance} from "fastify";
import {Organisation} from "../Models/Organisation.js";
import {User} from "../Models/Users.js";

export class OrganisationService {
    constructor() {}

    async createOrganisation(name: string, descripton: string): Promise<{ message: string; orgId: string }> {
        try {
            const newOrg = await Organisation.create({
                name: name,
                description: descripton,
                status: 'pending'
            });

            // @ts-ignore
            console.log("Organisation successfully created :: ", newOrg.id);

            // @ts-ignore
            return {message: "Organisation Created Successfully", orgId: newOrg.id};
        } catch (e){
            throw new Error("Organisation Creation Failed ::" , e);
        }
    }

    async onboardNewUser(username: string, email: string, role: string, organisation_id: string): Promise<void> {
        try {
            const newUser = await User.create({
                name: username,
                password: '123456',
                status: 'onboarded',
                type: role,
                organisationID: organisation_id,
                email: email
            })

            console.log("User Onboarded Successfully");
        } catch (e){
            throw new Error("User Onboarding  Failed" , e);
        }
    }
}