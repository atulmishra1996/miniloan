import {Loans} from "../Models/Loans.js";
import {Notification} from "../Models/Notification.js";
import {createNotificationMetaData} from "../Helpers/NotificationHelper.js";

export default class LoanService {

    constructor() {
    }

    async applyLoan(organisationId: string, userId: string, loanAmount: bigint, loanTenure: string, loanCurrency: string): Promise<void> {
        try {
            const loan = await Loans.create({
                userId: userId,
                orgId: organisationId,
                loanAmount: loanAmount,
                loanTenure: loanTenure,
                loanCurrency: loanCurrency,
                status: 'pending'
            })


            // @ts-ignore
            const notificationMetaData = createNotificationMetaData(loan.id, loanAmount, loanTenure);

            await Notification.create({
                userId: userId,
                type: 'loan',
                status: 'pending',
                organisationId: organisationId,
                metaData: notificationMetaData
            })

            //@ts-ignore
            return {message: "Loan Created Successfully  ", loanId: loan.id};
        } catch (error){
            throw new error(error);
        }
    }

    async approveLoan(loanId: string, approverId: string, approverRemarks: string){
        try {
            const loan = await Loans.findOne({
                where: {id: loanId}
            });

            if(loan){
                //@ts-ignore
                if(loan.status === 'pending') {
                    const approvingLoan = await Loans.update(
                        {status: 'approved' }, {
                        where: {id: loanId}
                    });

                    return {message: "Loan Successfully Approved - ",loanId: loanId};
                } else {
                    throw new Error("Loan is not in pending status " + loanId);
                }
            } else {
                throw new Error("Loan Doesn't Exist for id " + loanId);
            }

            //@ts-ignore
            return {message: "Loan Created Successfully", loanId: loan.id};
        } catch (error){
            throw new error(error);
        }
    }


    async rejectLoan(loanId: string, approverId: string, approverRemarks: string){
        try {
            const loan = await Loans.findOne({
                where: {id: loanId}
            });

            if(loan){
                //@ts-ignore
                if(loan.status === 'pending') {
                    const approvingLoan = await Loans.update(
                        {status: 'rejected' }, {
                            where: {id: loanId}
                        });

                    return {message: "Loan Successfully Approved - ",loanId: loanId};
                } else {
                    throw new Error("Loan is not in pending status " + loanId);
                }
            } else {
                throw new Error("Loan Doesn't Exist for id " + loanId);
            }

            //@ts-ignore
            return {message: "Loan Created Successfully", loanId: loan.id};
        } catch (error){
            throw new error(error);
        }
    }
}