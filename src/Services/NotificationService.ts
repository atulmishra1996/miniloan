import {Notification} from "../Models/Notification.js";
import {Model} from "sequelize";

export default class NotificationService {

    async fetchNotificationsByUser(userId: string): Promise<{
        message: string;
        notifications: any
    }> {
        try {
            const notifications = await Notification.findAll({
                where: {userId: userId}
            });

            return { message: "Notification Successfully Fetched", notifications: notifications}
        } catch (err){
            throw new Error(err);
        }
    }
}