import {sequelize} from "../plugins/db.js";
import {DataTypes} from "sequelize";

export const Organisation = sequelize.define("organisations", {
    id:{
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v4()'),
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    description: {
        type: DataTypes.STRING,
        allowNull: true
    },

    status: {
        type: DataTypes.ENUM('pending', 'onboarded'),
        allowNull: false
    },

    created_at: {
        type: DataTypes.DATE,
        allowNull: false
    },

    updated_at: {
        type: DataTypes.DATE,
        allowNull: false
    },
})

Organisation.sync().then(() => {
    console.log("Organisation table synced");
});
