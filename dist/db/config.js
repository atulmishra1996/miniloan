const env = process.env;
export const config = {
    db: {
        host: env.DB_HOST || '127.0.0.1',
        port: env.DB_PORT || '5432',
        user: env.DB_USER || 'atulmishra',
        password: env.DB_PASSWORD || 'atulmishra',
        database: env.DB_NAME || 'aspire',
        define: {
            // This option allows Sequelize to use UUIDs as primary keys for all models
            timestamps: true,
            freezeTableName: true,
            underscored: true,
            charset: 'utf8',
            dialectOptions: {
                collate: 'utf8_general_ci',
            },
        },
    },
    listPerPage: env.LIST_PER_PAGE || 10,
};
export const JWT_TOKEN = 'dajndakjbdyabdkhsabkjdsad';
//# sourceMappingURL=config.js.map