export declare const config: {
    db: {
        host: string;
        port: string;
        user: string;
        password: string;
        database: string;
        define: {
            timestamps: boolean;
            freezeTableName: boolean;
            underscored: boolean;
            charset: string;
            dialectOptions: {
                collate: string;
            };
        };
    };
    listPerPage: string | number;
};
export declare const JWT_TOKEN = "dajndakjbdyabdkhsabkjdsad";
