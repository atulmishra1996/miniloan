export declare function createNotificationMetaData(loanId: string, loanAmount: bigint, loanTenure: string): {
    loanId: string;
    loanAmount: bigint;
    loanTenure: string;
};
