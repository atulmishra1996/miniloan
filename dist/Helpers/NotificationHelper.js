export function createNotificationMetaData(loanId, loanAmount, loanTenure) {
    return {
        loanId: loanId,
        loanAmount: loanAmount,
        loanTenure: loanTenure
    };
}
//# sourceMappingURL=NotificationHelper.js.map