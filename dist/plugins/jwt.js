import fp from 'fastify-plugin';
import fastifyJwt from '@fastify/jwt';
export const jwtAuth = fp(async (fastify, opts) => {
    fastify.register(fastifyJwt, {
        secret: 'supersecret'
    });
    fastify.decorate('authenticate', async (request, reply) => {
        try {
            await request.jwtVerify();
        }
        catch (err) {
            reply.send(err);
        }
    });
});
//# sourceMappingURL=jwt.js.map