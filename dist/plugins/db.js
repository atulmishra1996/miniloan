import { config } from "../db/config.js";
import { Sequelize } from "sequelize";
export const sequelize = new Sequelize(config.db.database, config.db.user, config.db.password, {
    host: config.db.host,
    dialect: 'postgres',
    define: {
        timestamps: true,
        updatedAt: 'updated_at',
        createdAt: 'created_at',
    },
});
sequelize
    .authenticate()
    .then(() => console.log('Database connection has been established successfully.'))
    .catch((err) => console.error('Unable to connect to the database:', err));
//# sourceMappingURL=db.js.map