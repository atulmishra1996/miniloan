export default class NotificationController {
    notificationService;
    constructor(notificationService) {
        this.notificationService = notificationService;
    }
    async fetchNotifications(request, reply) {
        try {
            const notifications = await this.notificationService.fetchNotificationsByUser('12213');
            // @ts-ignore
            reply.code(201).send({ message: 'Successfully Fetched User Notifications', notifications: notifications });
        }
        catch (err) {
            reply.code(200).send({ message: 'Failed to fetch user notifications' + err.message });
        }
    }
}
//# sourceMappingURL=NotificationController.js.map