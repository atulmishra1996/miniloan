export default class UsersController {
    userService;
    constructor(userService) {
        this.userService = userService;
    }
    async signUp(request, reply) {
        try {
            console.log("came for sign up req");
            const { email, password, userName, organisationId, type } = request.body;
            const resultJson = await this.userService.signUp(userName, password, type, organisationId, email);
            return reply.code(201).send("User Created Successfully");
        }
        catch (err) {
            console.error("Error creating user:", err.message);
            return reply.code(409).send({ message: err.message });
        }
    }
    async logIn(request, reply) {
        const { email, password } = request.body;
        console.log("Controller : Checking if user exist :: " + email + " -- " + password);
        // Check if user exists
        try {
            const result = await this.userService.login(email, password);
            //@ts-ignore
            reply.code(200).send({ message: 'Login successful', token: result.token });
        }
        catch (error) {
            reply.code(500).send({ error: 'Login Failed - ' + error.message });
        }
    }
}
//# sourceMappingURL=UsersController.js.map