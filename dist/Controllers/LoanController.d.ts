import LoanService from "../Services/LoanService.js";
import { FastifyReply, FastifyRequest } from "fastify";
export default class LoanController {
    loanService: LoanService;
    constructor(loanService: LoanService);
    applyLoan(request: FastifyRequest, reply: FastifyReply): Promise<void>;
    approveLoan(request: FastifyRequest, reply: FastifyReply): Promise<void>;
    rejectLoan(request: FastifyRequest, reply: FastifyReply): Promise<void>;
}
