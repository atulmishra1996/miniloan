export default class LoanController {
    loanService;
    constructor(loanService) {
        this.loanService = loanService;
    }
    async applyLoan(request, reply) {
        try {
            const { organisationId, userId, loanAmount, loanTenure, loanCurrency } = request.body;
            const result = await this.loanService.applyLoan(organisationId, userId, loanAmount, loanTenure, loanCurrency);
            // @ts-ignore
            reply.code(201).send({ message: 'Loan Successfully created', loanId: result.loanId });
        }
        catch (err) {
            reply.code(200).send({ message: 'Loan Creation Failed' + err.message });
        }
    }
    async approveLoan(request, reply) {
        try {
            const { loanId, approverId, approverRemarks } = request.body;
            const result = await this.loanService.approveLoan(loanId, approverId, approverRemarks);
            // @ts-ignore
            reply.code(201).send({ message: 'Loan Approved Successfully', loanId: result.loanId });
        }
        catch (err) {
            reply.code(200).send({ message: 'Loan Approval Failed' + err.message });
        }
    }
    async rejectLoan(request, reply) {
        try {
            const { loanId, approverId, approverRemarks } = request.body;
            const result = await this.loanService.rejectLoan(loanId, approverId, approverRemarks);
            // @ts-ignore
            reply.code(201).send({ message: 'Loan Rejected Successfully', loanId: result.loanId });
        }
        catch (err) {
            reply.code(200).send({ message: 'Loan Rejection Failed' + err.message });
        }
    }
}
//# sourceMappingURL=LoanController.js.map