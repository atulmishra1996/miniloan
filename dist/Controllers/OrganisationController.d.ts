import { FastifyReply, FastifyRequest } from "fastify";
import { OrganisationService } from "../Services/OrganisationService.js";
export default class OrganisationController {
    organisationService: OrganisationService;
    constructor(organisationService: OrganisationService);
    create(request: FastifyRequest, reply: FastifyReply): Promise<void>;
}
