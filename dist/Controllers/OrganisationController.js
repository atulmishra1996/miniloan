export default class OrganisationController {
    organisationService;
    constructor(organisationService) {
        this.organisationService = organisationService;
    }
    async create(request, reply) {
        console.log("Getting Request :: " + request.body);
        const { organisationName, organisationDescription, userName, email } = request.body;
        try {
            const organisation = await this.organisationService.createOrganisation(organisationName, organisationDescription);
            const adminOnboarded = await this.organisationService.onboardNewUser(userName, email, 'admin', organisation.orgId);
            console.log("Result  :: ", organisation);
            reply.code(201).send({ message: 'Organisation and admin user created successfully.' });
        }
        catch (err) {
            reply.code(500).send(err);
        }
    }
}
//# sourceMappingURL=OrganisationController.js.map