import UsersService from "../Services/UsersService.js";
import { FastifyReply, FastifyRequest } from "fastify";
export default class UsersController {
    userService: UsersService;
    constructor(userService: UsersService);
    signUp(request: FastifyRequest, reply: FastifyReply): Promise<void>;
    logIn(request: FastifyRequest, reply: FastifyReply): Promise<void>;
}
