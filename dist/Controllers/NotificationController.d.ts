import NotificationService from "../Services/NotificationService.js";
import { FastifyReply, FastifyRequest } from "fastify";
export default class NotificationController {
    notificationService: NotificationService;
    constructor(notificationService: NotificationService);
    fetchNotifications(request: FastifyRequest, reply: FastifyReply): Promise<void>;
}
