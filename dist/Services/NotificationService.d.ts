export default class NotificationService {
    fetchNotificationsByUser(userId: string): Promise<{
        message: string;
        notifications: any;
    }>;
}
