import { Organisation } from "../Models/Organisation.js";
import { User } from "../Models/Users.js";
export class OrganisationService {
    constructor() { }
    async createOrganisation(name, descripton) {
        try {
            const newOrg = await Organisation.create({
                name: name,
                description: descripton,
                status: 'pending'
            });
            // @ts-ignore
            console.log("Organisation successfully created :: ", newOrg.id);
            // @ts-ignore
            return { message: "Organisation Created Successfully", orgId: newOrg.id };
        }
        catch (e) {
            throw new Error("Organisation Creation Failed ::", e);
        }
    }
    async onboardNewUser(username, email, role, organisation_id) {
        try {
            const newUser = await User.create({
                name: username,
                password: '123456',
                status: 'onboarded',
                type: role,
                organisationID: organisation_id,
                email: email
            });
            console.log("User Onboarded Successfully");
        }
        catch (e) {
            throw new Error("User Onboarding  Failed", e);
        }
    }
}
//# sourceMappingURL=OrganisationService.js.map