export declare class OrganisationService {
    constructor();
    createOrganisation(name: string, descripton: string): Promise<{
        message: string;
        orgId: string;
    }>;
    onboardNewUser(username: string, email: string, role: string, organisation_id: string): Promise<void>;
}
