import { Notification } from "../Models/Notification.js";
export default class NotificationService {
    async fetchNotificationsByUser(userId) {
        try {
            const notifications = await Notification.findAll({
                where: { userId: userId }
            });
            return { message: "Notification Successfully Fetched", notifications: notifications };
        }
        catch (err) {
            throw new Error(err);
        }
    }
}
//# sourceMappingURL=NotificationService.js.map