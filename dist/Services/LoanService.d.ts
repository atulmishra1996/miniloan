export default class LoanService {
    constructor();
    applyLoan(organisationId: string, userId: string, loanAmount: bigint, loanTenure: string, loanCurrency: string): Promise<void>;
    approveLoan(loanId: string, approverId: string, approverRemarks: string): Promise<{
        message: string;
        loanId: any;
    }>;
    rejectLoan(loanId: string, approverId: string, approverRemarks: string): Promise<{
        message: string;
        loanId: any;
    }>;
}
