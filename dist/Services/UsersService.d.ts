export default class UsersService {
    constructor();
    signUp(userName: string, password: string, type: string, orgId: string, email: string): Promise<{
        message: string;
    } | Error>;
    login(email: string, password: string): Promise<{
        message: string;
        token: string;
    } | Error>;
}
