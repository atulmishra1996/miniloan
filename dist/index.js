import fastify from "fastify";
import fastifyCors from "@fastify/cors";
import fastifySwagger from "@fastify/swagger";
import fastifySwaggerUI from "@fastify/swagger-ui";
import { jwtAuth } from "./plugins/jwt.js";
import OrganisationRoutes from "./Routes/OrganisationRoutes.js";
import OrganisationController from "./Controllers/OrganisationController.js";
import { OrganisationService } from "./Services/OrganisationService.js";
import UsersService from "./Services/UsersService.js";
import UsersController from "./Controllers/UsersController.js";
import UserRoutes from "./Routes/UserRoutes.js";
import LoanService from "./Services/LoanService.js";
import LoanController from "./Controllers/LoanController.js";
import LoanRoutes from "./Routes/Loan Routes.js";
import NotificationService from "./Services/NotificationService.js";
import NotificationController from "./Controllers/NotificationController.js";
import NotificationRoutes from "./Routes/NotificationRoutes.js";
const server = fastify({ logger: true });
server.register(fastifyCors);
server.register(fastifySwagger, {});
server.register(fastifySwaggerUI, {
    routePrefix: '/docs',
    uiConfig: {
        docExpansion: 'full',
        deepLinking: false
    },
    uiHooks: {
        onRequest: function (request, reply, next) {
            next();
        },
        preHandler: function (request, reply, next) {
            next();
        }
    },
    staticCSP: true,
    transformStaticCSP: (header) => header,
    transformSpecification: (swaggerObject, request, reply) => {
        return swaggerObject;
    },
    transformSpecificationClone: true
});
server.register(jwtAuth);
const organisationService = new OrganisationService();
const organisationController = new OrganisationController(organisationService);
const organisationRoutes = new OrganisationRoutes(organisationController);
const userService = new UsersService();
const userController = new UsersController(userService);
const userRoutes = new UserRoutes(userController);
const loanService = new LoanService();
const loanController = new LoanController(loanService);
const loanRoutes = new LoanRoutes(loanController);
const notificationService = new NotificationService();
const notificationController = new NotificationController(notificationService);
const notificationRoutes = new NotificationRoutes(notificationController);
server.register(organisationRoutes.routes.bind(organisationRoutes));
server.register(userRoutes.routes.bind(userRoutes));
server.register(loanRoutes.routes.bind(loanRoutes));
server.register(notificationRoutes.routes.bind(notificationRoutes));
server.listen(3000, (err, address) => {
    if (err) {
        server.log.error(err);
        process.exit(1);
    }
    server.log.info(`Server listening at ${address}`);
});
//# sourceMappingURL=index.js.map