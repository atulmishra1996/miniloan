export default class NotificationRoutes {
    notificationController;
    constructor(notificationController) {
        this.notificationController = notificationController;
    }
    async routes(server) {
        server.get("/notifications", {
            schema: {
                summary: "Fetch all notification for a user",
            },
        }, this.notificationController.fetchNotifications.bind(this.notificationController));
    }
}
//# sourceMappingURL=NotificationRoutes.js.map