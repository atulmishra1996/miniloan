import OrganisationController from "../Controllers/OrganisationController.js";
export default class OrganisationRoutes {
    organisationController: OrganisationController;
    /**
     * @constructor
     *
     * @param {OrganisationController} organisationController
     */
    constructor(organisationController: OrganisationController);
    routes(server: any): Promise<void>;
}
