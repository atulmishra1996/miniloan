import NotificationController from "../Controllers/NotificationController.js";
export default class NotificationRoutes {
    notificationController: NotificationController;
    constructor(notificationController: NotificationController);
    routes(server: any): Promise<void>;
}
