export default class OrganisationRoutes {
    organisationController;
    /**
     * @constructor
     *
     * @param {OrganisationController} organisationController
     */
    constructor(organisationController) {
        this.organisationController = organisationController;
    }
    async routes(server) {
        server.post("/organisation", {
            schema: {
                summary: "Creates a new Organisation and onboards first admin",
                body: {
                    type: "object",
                    properties: {
                        organisationName: { type: "string" },
                        organisationDescription: { type: "string" },
                        userName: { type: "string" },
                        email: { type: "string" }
                    }
                },
            },
        }, this.organisationController.create.bind(this.organisationController));
    }
}
//# sourceMappingURL=OrganisationRoutes.js.map