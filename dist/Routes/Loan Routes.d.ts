import LoanController from "../Controllers/LoanController.js";
export default class LoanRoutes {
    loanController: LoanController;
    constructor(loanController: LoanController);
    routes(server: any): Promise<void>;
}
