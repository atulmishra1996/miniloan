import UsersController from "../Controllers/UsersController.js";
export default class UserRoutes {
    userController: UsersController;
    constructor(userController: UsersController);
    routes(server: any): Promise<void>;
}
