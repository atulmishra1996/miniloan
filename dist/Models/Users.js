import { sequelize } from "../plugins/db.js";
import { DataTypes } from "sequelize";
export const User = sequelize.define("users", {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status: {
        type: DataTypes.ENUM('pending', 'onboarded'),
        allowNull: true
    },
    type: {
        type: DataTypes.ENUM('admin', 'member', 'customer'),
        allowNull: true
    },
    organisationID: {
        type: DataTypes.UUID,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false
    },
});
User.sync().then(() => {
    console.log("Users table synced");
});
//# sourceMappingURL=Users.js.map