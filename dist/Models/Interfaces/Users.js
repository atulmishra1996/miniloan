export const createUserTable = `
CREATE TABLE IF NOT EXISTS users (
  id UUID PRIMARY KEY,
  username VARCHAR(255) UNIQUE NOT NULL,
  password VARCHAR(255) NOT NULL,
  role VARCHAR(50) NOT NULL,
  organisation_id UUID REFERENCES organisations(id)
);
`;
//# sourceMappingURL=Users.js.map