export interface User {
    id: string;
    username: string;
    password: string;
    role: 'admin' | 'user';
    organisation_id: string;
}
export declare const createUserTable = "\nCREATE TABLE IF NOT EXISTS users (\n  id UUID PRIMARY KEY,\n  username VARCHAR(255) UNIQUE NOT NULL,\n  password VARCHAR(255) NOT NULL,\n  role VARCHAR(50) NOT NULL,\n  organisation_id UUID REFERENCES organisations(id)\n);\n";
