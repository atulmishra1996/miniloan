export interface OrganisationI {
    id: string;
    name: string;
    description: string;
}
export declare const createOrganisationTable = "\nCREATE TABLE IF NOT EXISTS organisations (\n  id UUID PRIMARY KEY,\n  name VARCHAR(255) NOT NULL,\n  description VARCHAR(255) NOT NULL.\n);\n";
