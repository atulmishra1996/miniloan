export const createOrganisationTable = `
CREATE TABLE IF NOT EXISTS organisations (
  id UUID PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255) NOT NULL.
);
`;
//# sourceMappingURL=Organisation.js.map