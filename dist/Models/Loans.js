import { sequelize } from "../plugins/db.js";
import { DataTypes } from "sequelize";
export const Loans = sequelize.define("loans", {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    userId: {
        type: DataTypes.UUID,
        allowNull: true
    },
    orgId: {
        type: DataTypes.UUID,
        allowNull: true
    },
    loanAmount: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    loanCurrency: {
        type: DataTypes.ENUM('INR', 'US'),
        allowNull: true
    },
    loanTenure: {
        type: DataTypes.ENUM('3', '6', '9'),
        allowNull: true
    },
    status: {
        type: DataTypes.ENUM('pending', 'approved', 'rejected'),
        allowNull: false
    },
    approverId: {
        type: DataTypes.UUID,
        allowNull: true
    },
    approverRemarks: {
        type: DataTypes.STRING,
        allowNull: true
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false
    },
});
Loans.sync().then(() => {
    console.log("Loans table synced");
});
//# sourceMappingURL=Loans.js.map