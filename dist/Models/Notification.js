import { sequelize } from "../plugins/db.js";
import { DataTypes } from "sequelize";
export const Notification = sequelize.define("notifications", {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    type: {
        type: DataTypes.ENUM('loan', 'onboarding'),
        allowNull: false
    },
    status: {
        type: DataTypes.ENUM('pending', 'completed'),
        allowNull: false
    },
    userId: {
        type: DataTypes.UUID,
        allowNull: false
    },
    organisationId: {
        type: DataTypes.UUID,
        allowNull: false
    },
    metaData: {
        type: DataTypes.JSON,
        allowNull: false
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false
    },
});
Notification.sync().then(() => {
    console.log("Notifications table synced");
});
//# sourceMappingURL=Notification.js.map